#include "terminal.h"

Terminal::Terminal(QWidget *parent) : QPlainTextEdit (parent), logFile (NULL)
{
	pText = new QPlainTextEdit ();
	QFont font;
	font.setFamily (QString::fromUtf8("Courier New"));
	font.setPointSize (10);
	pText->setFont (font);
	pText->setFrameShape (QFrame::Panel);
	pText->setLineWidth (1);
	pText->setUndoRedoEnabled (false);
	pText->setReadOnly (true);
	pText->setOverwriteMode (false);
	pText->setTextInteractionFlags (Qt::TextSelectableByMouse);
	pText->setMaximumBlockCount (10000);

	// Create Terminal history class
	termHist = new TerminalHistory ();
	// Create Terminal commands class
	termCom = new TerminalCommands (this, this);
	addTimeStamp();
}

Terminal::~Terminal ()
{
}

void Terminal::backspace ()
{
	moveCursor (QTextCursor::End, QTextCursor::MoveAnchor );
	moveCursor (QTextCursor::Left, QTextCursor::MoveAnchor);
	QPoint p = getRowColumn();
	int len = p.x ();
	if (len >= stampLength)
	{
		moveCursor (QTextCursor::End, QTextCursor::KeepAnchor );
		textCursor ().removeSelectedText();
	}
	else
		moveCursor (QTextCursor::End, QTextCursor::MoveAnchor );
}

void Terminal::addToHistory ()
{
	QString str = getTypedCmd ();
	termHist->addToHistory (str);
}

void Terminal::nextOnCmdHistory ()
{
	nextCmdHist = termHist->nextOnCmdHistory ();
}

void Terminal::previousOnCmdHistory ()
{
	nextCmdHist = termHist->previousOnCmdHistory ();
}

void Terminal::updateCmdHistorydOnScreen ()
{
	moveCursor (QTextCursor::End, QTextCursor::MoveAnchor );
	QPoint p = getRowColumn();
	int len = p.x ();
	while (len > stampLength)
	{
		moveCursor (QTextCursor::Left, QTextCursor::MoveAnchor );
		p = getRowColumn();
		len = p.x();
	}

	moveCursor (QTextCursor::End, QTextCursor::KeepAnchor );
	textCursor ().removeSelectedText();

	cmdHist = nextCmdHist;
	insertPlainText (cmdHist);
	nextCmdHist.clear();
}

QString Terminal::getTypedCmd ()
{
	moveCursor (QTextCursor::End, QTextCursor::MoveAnchor);
	QPoint p = getRowColumn();
	int len = p.x ();
	while (len > stampLength)
	{
		moveCursor (QTextCursor::Left, QTextCursor::MoveAnchor);
		p = getRowColumn();
		len = p.x();
	}
	moveCursor (QTextCursor::End, QTextCursor::KeepAnchor);
	QString str = textCursor().selectedText();
	moveCursor (QTextCursor::End, QTextCursor::MoveAnchor);
	return str;
}

void Terminal::updateTypedCmd (QString str)
{
	moveCursor (QTextCursor::End, QTextCursor::MoveAnchor);
	QPoint p = getRowColumn();
	int len = p.x ();
	while (len > stampLength)
	{
		moveCursor (QTextCursor::Left, QTextCursor::MoveAnchor);
		p = getRowColumn();
		len = p.x();
	}
	moveCursor (QTextCursor::End, QTextCursor::KeepAnchor);
	QString typedCmd = textCursor().selectedText();
	typedCmd += str;
	textCursor().removeSelectedText();
	insertPlainText (typedCmd);
}

void Terminal::addTimeStamp ()
{
	QDateTime timeStamp = QDateTime::currentDateTime ();
	QPoint p = getRowColumn();
	int y = p.y ();
	int x = p.x ();
	if (y >= 1 && x > 0)
		insertPlainText ("\n");
	QString str = "[" + timeStamp.toString ("hh:mm:ss,zzz") + "]$ ";
	stampLength = str.length ();
	insertPlainText (str);
}

void Terminal::addTimeStampCR ()
{
	QDateTime timeStamp = QDateTime::currentDateTime ();
	QString str = "[" + timeStamp.toString ("hh:mm:ss,zzz") + "]$ ";
	stampLength = str.length ();
	insertPlainText (str);
}

QPoint Terminal::getRowColumn()
{
	QPoint xy;
	QTextCursor cursor = textCursor();
	xy.setX (cursor.columnNumber());
	xy.setY (cursor.blockNumber() + 1);
	return xy;
}

void Terminal::clearScreen ()
{
	clear ();
}

void Terminal::listHistory ()
{
	addToHistory ();
	QStringList histList = termHist->listHistory ();
	int loop;
	QString pos;
	for (loop = 0; loop < histList.length (); loop++)
	{
		insertPlainText ("\n");
		pos = "  ";
		pos += QString::number (loop + 1);
		pos += "  " + histList.at (loop);
		insertPlainText (pos);
	}
}

// Reimplemented method to capture the log
void Terminal::insertPlainText (const QString &text)
{
	if (logFile)
	{
		logFile->write (text.toAscii());
		logFile->flush ();
	}
	QPlainTextEdit::insertPlainText (text);
}

void Terminal::saveScreen ()
{
	QString name = QFileDialog::getSaveFileName (this, "Save Screen", QDir::currentPath(), "All Files (*)");
	QFile file (name);
	if (!file.open (QIODevice::WriteOnly))
	{
		QString str ("Cannot write file ");
		str += name;
		QMessageBox::critical (this,"File read error", str);
		return;
	}
	file.write (toPlainText().toAscii());
	file.close();
}

void Terminal::startLogging ()
{
	if (logFile)
	{
		QMessageBox::critical (this, "Error", "Logging already active");
		return;
	}

	QString name = QFileDialog::getSaveFileName (this, "Select log file", QDir::currentPath (), "All Files (*)");
	if (name.length () == 0)
		return;
	logFile = new QFile (name);
	if (!logFile->open (QIODevice::WriteOnly))
	{
		QString str ("Cannot write file ");
		str += name;
		QMessageBox::critical (this,"File read error", str);
		logFile = NULL;
		return;
	}
}

void Terminal::endLogging ()
{
	if (logFile)
	{
		logFile->close ();
		delete logFile;
		logFile = NULL;
	}
}

bool Terminal::parseCommand (QString str)
{
	return termCom->parseCommand (str);
}
