#ifndef TERMINAL_STYLE_H
#define TERMINAL_STYLE_H

#include <QDialog>
#include "terminal.h"

class Terminal;
namespace Ui
{
	class TerminalStyle;
}

class TerminalStyle : public QDialog
{
		Q_OBJECT

	public:
		explicit TerminalStyle (QWidget *parent = 0, Terminal *t = 0);
		virtual ~TerminalStyle ();
		Terminal *term;
		void loadConfiguration ();
		void applySettings ();

	private slots:
		void on_pushButtonBackGround_clicked();
		void on_pushButtonFontColor_clicked();
		void on_pushButtonFontType_clicked();
		void on_buttonBox_accepted();
		void on_pushButtonApply_clicked();
		void on_pushButtonRestoreDefault_clicked();

		void on_buttonBox_rejected();

	private:
		Ui::TerminalStyle *ui;
		QPalette *pal;
};

#endif // TERMINAL_STYLE_H
