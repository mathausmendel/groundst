#ifndef TERMINAL_HISTORY_H
#define TERMINAL_HISTORY_H

#include <QObject>
#include <QPlainTextEdit>
#include <QStringList>
#include <QString>
#include <QSettings>

class TerminalHistory : public QObject
{
		Q_OBJECT
	public:
		explicit TerminalHistory (QObject *parent = 0);
		void addToHistory (QString cmd);
		virtual ~TerminalHistory ();
		QString nextOnCmdHistory ();
		QString previousOnCmdHistory ();
		QStringList listHistory ();

	signals:

	public slots:

	private:
		QStringList cmdHistory;
		int historySize;
		int histIndex;
};

#endif // TERMINAL_HISTORY_H
