#include "terminalcommands.h"
#include <QDebug>

commands TerminalCommands::cmds[] =
{
	{"clear", &TerminalCommands::clear},
	{"history", &TerminalCommands::history},
	{"version", &TerminalCommands::version},
	{"exit", &TerminalCommands::exit},
	{"help", &TerminalCommands::help}
};

TerminalCommands::TerminalCommands (QObject *parent, Terminal *t) :QObject(parent)
{
	term = t;
}

TerminalCommands::~TerminalCommands ()
{
}

void TerminalCommands::help ()
{
//	term->insert ();
}

void TerminalCommands::clear ()
{
	term->clearScreen ();
}

void TerminalCommands::exit ()
{
//	this->exit ();
	term->close ();
}

void TerminalCommands::version ()
{

}

void TerminalCommands::history ()
{
	term->listHistory ();
}

bool TerminalCommands::parseCommand (const QString &str)
{
	bool ret = false;

	for (std::size_t i = 0; i != sizeof (cmds) / sizeof (cmds[0]); ++i)
	{
		if(str == cmds[i].name)
		{
			(this->*cmds[i].funct)();
			ret = true;
			break;
		}
	}

	return ret;
}
