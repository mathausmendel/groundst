#include "terminalhistory.h"

TerminalHistory::TerminalHistory(QObject *parent) : QObject(parent)
{
	historySize = 10;
	histIndex = 0;
	QSettings settings (QSettings::UserScope, "groundST", "history");
	if (settings.contains ("history"))
	{
		cmdHistory.append (settings.value ("history").toStringList ());
		historySize = settings.value ("historySize").toInt ();
	}
	else
		historySize = 300;

}

TerminalHistory::~TerminalHistory()
{
}

void TerminalHistory::addToHistory (QString cmd)
{
	if (cmdHistory.length() < historySize)
		cmdHistory.append (cmd);
	else
	{
		cmdHistory.removeFirst ();
		cmdHistory.append (cmd);
	}
	histIndex = 0;
	QSettings settings (QSettings::UserScope, "groundST", "history");
	settings.setValue ("history", cmdHistory);
}

QString TerminalHistory::nextOnCmdHistory ()
{
	QString ret;
	histIndex--;
	if (histIndex < 0)
	{
		histIndex = cmdHistory.length() - 1;
		ret = cmdHistory.at (histIndex);
	}
	else
	{
		ret = cmdHistory.at (histIndex);
	}
	return ret;
}

QString TerminalHistory::previousOnCmdHistory ()
{
	QString ret;
	histIndex++;
	if (histIndex >= cmdHistory.length())
	{
		histIndex = 0;
		ret = cmdHistory.at (histIndex);
	}
	else
	{
		ret = cmdHistory.at (histIndex);
	}
	return ret;
}

QStringList TerminalHistory::listHistory ()
{
	return cmdHistory;
}
