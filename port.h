#ifndef PORT_H
#define PORT_H

#include <QObject>
#include <QStringList>
#include <QSettings>
#include <QDir>
#include "qextserialenumerator.h"
#include "qextserialport.h"

class Port : public QObject
{
		Q_OBJECT
	public:
		explicit Port(QObject *parent = 0);
		virtual ~Port();
		void listAvailablePorts ();
		void setupBaud ();
		void setupParity ();
		void setupFlowControl ();
		void setupDataBits ();
		void setupStopBits ();
		QStringList getBaud ();
		QStringList getParity ();
		QStringList getFlow ();
		QStringList getDataBits ();
		QStringList getStopBits ();
		QStringList getPorts ();
		struct po
		{
			int port;
			int baud;
			int parity;
			int flow;
			int dataBits;
			int stopBits;
		};
		po portS;
		void readPortConfig (po *p);
		void savePortConfig (po *p);
		int connectDisconnect ();
		void sendBytes (QString b);
		int pollSerial();
		QByteArray receivedBuffer;

		bool isOpen ();


	signals:

	public slots:

	private:
		int intBaud[8];
		QStringList stringListBaud;
		int intParity[6];
		QStringList stringListParity;
		int intFlow[4];
		QStringList stringListFlowControl;
		int intDataBits[5];
		QStringList stringListDataBits;
		int intStopBits[4];
		QStringList stringListStopBits;
		QStringList stringListPorts;
		QextSerialPort *uartPort;

};

#endif // PORT_H
