#ifndef TERMINALSETTINGS_H
#define TERMINALSETTINGS_H

#include <QDialog>
#include <QSettings>

namespace Ui {
	class TerminalSettings;
}

class TerminalSettings : public QDialog
{
		Q_OBJECT

	public:
		explicit TerminalSettings (QWidget *parent = 0);
		virtual ~TerminalSettings ();

	private slots:
		void on_buttonBox_accepted();

	private:
		Ui::TerminalSettings *ui;
		void loadConfiguration ();
};

#endif // TERMINALSETTINGS_H
