#ifndef TERMINAL_H
#define TERMINAL_H

#include <QPlainTextEdit>
#include <QString>
#include <QDateTime>
#include <QPoint>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include "terminalhistory.h"
#include "terminalcommands.h"

class TerminalCommands;
class Terminal : public QPlainTextEdit
{
		Q_OBJECT
	public:
		explicit Terminal(QWidget *parent = 0);
		virtual ~Terminal ();
		void backspace ();
		void addToHistory ();
		void addTimeStamp ();
		void addTimeStampCR ();
		void nextOnCmdHistory ();
		void previousOnCmdHistory ();
		void updateCmdHistorydOnScreen ();
		QPoint getRowColumn ();
		QString getTypedCmd ();
		void updateTypedCmd (QString str);
		bool parseCommand (QString str);
		void clearScreen ();
		void listHistory ();

	signals:

	public slots:
		void insertPlainText (const QString &text);
		void saveScreen ();
		void startLogging ();
		void endLogging ();

	private:
		QPlainTextEdit *pText;
		QFile *logFile;
		QString cmd;
		void insertCmd ();
		TerminalHistory *termHist;
		void clearTypedCmd ();
		QString cmdHist;
		QString nextCmdHist;
		int stampLength;
		TerminalCommands *termCom;
};

#endif // TERMINAL_H
